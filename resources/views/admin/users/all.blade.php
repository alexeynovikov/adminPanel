@extends('admin.layout')
@section('page')
    <div class="page-header">
        <h1 class="page-title">This a page title</h1>

        <div class="breadcrumb">
            BreadCrumb here
        </div>

        <div class="page-header-actions">
            <a class="btn btn-sm btn-primary btn-round waves-effect waves-classic" href="http://js-grid.com" target="_blank">
                <i class="icon md-plus" aria-hidden="true"></i>
                <span class="hidden-sm-down">New</span>
            </a>
        </div>
    </div>

    <div class="col-xxl-6">
        <div class="panel">
            <header class="panel-heading">
                <h3 class="panel-title">Users</h3>
            </header>
            <div class="panel-body">
                <div id="userGrid"></div>
            </div>
        </div>
    </div>

@endsection

@section('ap-page-plugins-css')
    <link rel="stylesheet" href="{{asset('css/global/vendor/jsgrid/jsgrid.css')}}">
@endsection

@section('ap-page-plugins-js')
    <script src="{{asset('css/global/vendor/jsgrid/jsgrid.js')}}"></script>
@endsection

@section('ap-page-footer-js')
    <script>
        $(document).ready(function () {
            Site.run();

            $('#userGrid').jsGrid({
                height: "500px",
                width: "100%",

                sorting: true,
                paging: false,
                autoload: true,

                controller: {
                    loadData: function() {
                        var d = $.Deferred();

                        $.ajax({
                            url: "https://randomuser.me/api/",
                            dataType: "json"
                        }).done(function(response) {
                            console.log(response);
                            d.resolve(response.value);
                        });

                        return d.promise();
                    }
                },

                fields: [{
                    name: "Name",
                    type: "text"
                }, {
                    name: "Description",
                    type: "textarea",
                    width: 150
                }, {
                    name: "Rating",
                    type: "number",
                    width: 50,
                    align: "center",
                    itemTemplate: function(value) {
//                        return $("<div>").addClass("rating text-nowrap").append(new Array(value + 1).join('<i class="icon md-star orange-600 mr-3"></i>'));
                    }
                }, {
                    name: "Price",
                    type: "number",
                    width: 50,
                    itemTemplate: function(value) {
                        return value.toFixed(2) + "$";
                    }
                }]
            });
        });
    </script>
@endsection