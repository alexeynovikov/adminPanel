@include('admin.layouts.header');
<body class="animsition dashboard site-menubar-unfold site-menubar-keep">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
@include('admin.layouts.nav')
<!-- Page -->
<div class="page">
    @yield('page')
</div>
<!-- End Page -->
@include('admin.layouts.footer')