<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">© 2017 <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
    <div class="site-footer-right">
        Crafted with <i class="red-600 icon md-favorite"></i> by <a href="http://themeforest.net/user/amazingSurge">amazingSurge</a>
    </div>
</footer>
<!-- Core  -->
<script src="{{asset('js/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="{{asset('js/global/vendor/jquery/jquery.js')}}"></script>
<script src="{{asset('js/global/vendor/tether/tether.js')}}"></script>
<script src="{{asset('js/global/vendor/bootstrap/bootstrap.js')}}"></script>
<script src="{{asset('js/global/vendor/animsition/animsition.js')}}"></script>
<script src="{{asset('js/global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{asset('js/global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{asset('js/global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="{{asset('js/global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>
<script src="{{asset('js/global/vendor/waves/waves.js')}}"></script>
@yield('ap-page-core-js')
<!-- Plugins -->
<script src="{{asset('js/global/vendor/switchery/switchery.min.js')}}"></script>
<script src="{{asset('js/global/vendor/intro-js/intro.js')}}"></script>
<script src="{{asset('js/global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('js/global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{asset('js/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
<script src="{{asset('js/global/vendor/peity/jquery.peity.min.js')}}"></script>
@yield('ap-page-plugins-js')
<!-- Scripts -->
<script src="{{asset('js/global/js/State.js')}}"></script>
<script src="{{asset('js/global/js/Component.js')}}"></script>
<script src="{{asset('js/global/js/Plugin.js')}}"></script>
<script src="{{asset('js/global/js/Base.js')}}"></script>
<script src="{{asset('js/global/js/Config.js')}}"></script>
<script src="{{asset('js/Section/Menubar.js')}}"></script>
<script src="{{asset('js/Section/GridMenu.js')}}"></script>
<script src="{{asset('js/Section/Sidebar.js')}}"></script>
<script src="{{asset('js/Section/PageAside.js')}}"></script>
<script src="{{asset('js/Plugin/menu.js')}}"></script>
<script src="{{asset('js/global/js/config/colors.js')}}"></script>
<script src="{{asset('js/config/tour.js')}}"></script>
<script>
    Config.set('assets', '../assets');
</script>
<!-- Page -->
<script src="{{asset('js/Site.js')}}"></script>
<script src="{{asset('js/global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{asset('js/global/js/Plugin/slidepanel.js')}}"></script>
<script src="{{asset('js/global/js/Plugin/switchery.js')}}"></script>
<script src="{{asset('js/global/js/Plugin/matchheight.js')}}"></script>
<script src="{{asset('js/global/js/Plugin/jvectormap.js')}}"></script>
<script src="{{asset('js/global/js/Plugin/peity.js')}}"></script>
@yield('ap-page-footer-js')
</body>
</html>